import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import  HomeScreen  from './Screens/home_screen'
import  GameScreen  from './Screens/game_screen'

import { createStackNavigator, createAppContainer } from 'react-navigation';



const AppNavigator = createStackNavigator({
  Home: {
    screen: HomeScreen,
  },
  Game:{
    screen: GameScreen,
  }
  },{
    initialRouteName: 'Home',
    headerMode: 'none'
  });

export default createAppContainer(AppNavigator);


