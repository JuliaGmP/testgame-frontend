import React from 'react';
import { StyleSheet, Text, TouchableHighlight, Dimensions } from 'react-native';

export default class AnswerButton extends React.Component{

    updateStyle = function(CA,E,P) {
        if(CA && !E){
            return{
            borderColor: "#000066",
            backgroundColor: "green",
            borderWidth: 1,
            borderRadius: 10
         }
     }
        else if(P && !CA){
            return{
                borderColor: "#000066",
                backgroundColor: "red",
                borderWidth: 1,
                borderRadius: 10
             }
        }
      }
    
  render() {
    return (
        <TouchableHighlight onPress={this.props.onPress} style={this.updateStyle(this.props.isCorrectAnswer,this.props.enabled,this.props.answerPressed)}>
 
            <Text style={MyButtonStyle}>{this.props.text}</Text>
        </TouchableHighlight>
    );
  }
}

const MyButtonStyle = {
    fontSize: 30,
    alignItems: 'center',
    justifyContent: 'center',
    margin: 1,
    height: Dimensions.get('window').width/5, 
    borderWidth: 1,
    borderColor: '#8b0000',
    textAlign: 'center',
}
