import React from 'react';
import { StyleSheet, Text, TouchableHighlight, Dimensions } from 'react-native';

export default class MyButton extends React.Component{
  render() {
    return (
        <TouchableHighlight onPress={this.props.onPress}>
            <Text >{this.props.text}</Text>
        </TouchableHighlight>
    );

  }

}

