import React from 'react';
import { ActivityIndicator, FlatList, StyleSheet, Text, View, Button, TouchableHighlight } from 'react-native';
import AnswerButton from './Components/answer_button';

export default class GameScreen extends React.Component{

  constructor(props) {
    super(props);

    this.state = {
      isLoading: true,
      question: [],
      questionIndex: 0,
      ArrayUserAnswers: [],
      buttonEnabled: true,
      numberQuestions: 0,
      answerPressed: null
    };
  }

  componentDidMount() { 

    this.getQuestions().then((questions) =>{

      this.setState({ question: questions})
      this.setState({ isLoading: false})
      this.setState({ numberQuestions: questions.length -1})
                      
    })
  }

  async getQuestions(){
    let url ='http://192.168.1.48:3000/games/Game1';
    let url2 = 'https://facebook.github.io/react-native/movies.json';
    let response = await fetch(url);
    var responseJson = await response.json();
    return responseJson.objetcGameBody;
  }

  async addAnswer(index){
    if(this.state.buttonEnabled){
      let state = this.state.ArrayUserAnswers;
      await state.push(index)
      await this.setState( {answerPressed : index});
      await this.setState({ ArrayUserAnswers : state});
      await this.setState({ buttonEnabled : false})
      } 
  }

  isCorrectAnswer(index){
    console.log(this.state.question[this.state.questionIndex].numberCorrectAnswer)
    console.log(index)

    if (index === this.state.question[this.state.questionIndex].numberCorrectAnswer){
      console.log("Se dedtecto")
      return true;
    } else {
      return false;
    }

  }

  render() {
    if(this.state.isLoading){
      return(
        <View style={{flex: 1, padding: 20}}>
          <ActivityIndicator/>
        </View>
      )
    }
   
    const { question } = this.state;
            
    return (
      <View style={{flex:1, alignItems:'center'}}>
        <View style ={questionStyle}>
          <Text style={{fontSize: 40, textAlign: 'center'}}>{question[this.state.questionIndex].stringQuestion}</Text>
        </View>
        <View style ={answersStyle}>

          <AnswerButton text={question[this.state.questionIndex].arrayAnswers[0]} answerPressed={(this.state.answerPressed === 0)} isCorrectAnswer={this.isCorrectAnswer(0)} enabled={this.state.buttonEnabled} onPress={() => {this.addAnswer(0);}}/>
          <AnswerButton text={question[this.state.questionIndex].arrayAnswers[1]} answerPressed={(this.state.answerPressed === 1)} isCorrectAnswer={this.isCorrectAnswer(1)} enabled={this.state.buttonEnabled} onPress={() => {this.addAnswer(1);}}/>
          <AnswerButton text={question[this.state.questionIndex].arrayAnswers[2]} answerPressed={(this.state.answerPressed === 2)} isCorrectAnswer={this.isCorrectAnswer(2)} enabled={this.state.buttonEnabled} onPress={() => {this.addAnswer(2);}}/>
          <AnswerButton text={question[this.state.questionIndex].arrayAnswers[3]} answerPressed={(this.state.answerPressed === 3)} isCorrectAnswer={this.isCorrectAnswer(3)} enabled={this.state.buttonEnabled} onPress={() => {this.addAnswer(3);}}/>

          <AnswerButton text={"Siguiente"} onPress={() => {
            this.setState({ buttonEnabled : true});
            this.setState({ answerPressed : null});

            if (this.state.questionIndex=== this.state.numberQuestions ) {
              this.props.navigation.goBack()
            } else {
              this.setState({ questionIndex : this.state.questionIndex + 1});            
            }
     
            }}/> 
        </View>
      </View>
    );
  }
}

const questionStyle = {
  alignSelf: 'stretch',
  textAlign: 'center',
  flex: 1,
  marginTop:   15,
  borderWidth: 1,
  borderColor: '#8b0000',
  flex: 1,
  justifyContent:'center',
};

const answersStyle = {
  alignSelf: 'stretch',
  textAlign: 'center',
  flex: 2,
  marginTop:   15,
  marginBottom:  15,
  borderWidth: 1,
  borderColor: '#8b0000'
};
