import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import MyButton from './Components/my_button';
import { NavigationScreenProp } from 'react-navigation';
import AnswerButton from './Components/answer_button';
import { ScrollView } from 'react-native-gesture-handler';


export default class HomeScreen extends React.Component{
  render() {
    return (
        <View style={{flex:1, alignItems:'center', justifyContent:'space-around'}}>
        <ScrollView >
        <MyButton 
          onPress={() => this.props.navigation.navigate('Game')}
          text={"Game 1"}/>
          <MyButton 
          onPress={() => this.props.navigation.navigate('Game')}
          text={"Game 2"}/>
          <MyButton 
          onPress={() => this.props.navigation.navigate('Game')}
          text={"Game 3"}/>
          <MyButton 
          onPress={() => this.props.navigation.navigate('Game')}
          text={"Game 4"}/>
          
        </ScrollView>  
         
        </View>
    );

  }

}


const ScrollViewStyle = {

  alignItems: 'center',
};